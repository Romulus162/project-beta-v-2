from django.urls import path
from .views import (
    api_list_salespeople,
    api_list_sales,
    api_salesperson,
    api_list_customers,
    api_customer,
    api_sale,
    api_cars_update
    )

urlpatterns = [
    path("salespeople/", api_list_salespeople, name="api_list_salespeople"),
    path("salespeople/<int:id>/", api_salesperson, name="delete_salesperson"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:id>", api_customer, name="api_customers"),
    path("sales/<int:id>", api_sale, name="api_sale"),
    path("sales/<str:vin>/", api_cars_update, name="cars_update"),

]
