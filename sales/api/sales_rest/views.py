from django.shortcuts import render
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import Salesperson, Customer, Sale, AutomobileVO

# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "salesperson",
        "customer",
        "automobile",
        "price",
        "id"
    ]
    encoders = {
        "salesperson": SalespersonEncoder(),
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)

        customer = Customer.objects.create(**content)
        return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )


@require_http_methods(["GET", "DELETE"])
def api_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"}
            )
    else:
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse({"salesperson": salesperson}, encoder=SalespersonEncoder)
        except:
            return JsonResponse({"message": "Could not create a salesperson. Employee ID must be a unique integer."}, status=400)


@require_http_methods(["GET", "DELETE"])
def api_salesperson(request, id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist"}
            )
    else:
        try:
            salesperson = Salesperson.objects.get(id=id)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == 'GET':
        try:
            sales = Sale.objects.all()
            return JsonResponse({"sales": sales}, encoder=SaleEncoder)
        except:
            return JsonResponse({"message": "Could not get the sales."}, status=400)
    else:
        content = json.loads(request.body)
        try:
            content_vin = content['automobile']
            automobile = AutomobileVO.objects.get(vin=content_vin)
            if automobile.sold:
                return JsonResponse({"message": "Automobile is already sold."}, status=400)
            content['automobile'] = automobile
            automobile.sold = True
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Automobile does not exist."}, status=400)
        try:
            content_id = content['salesperson']
            salesperson = Salesperson.objects.get(id=content_id)
            content['salesperson'] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Sales person does not exist."}, status=400)
        try:
            content_name = content['customer']
            customer = Customer.objects.get(id=content_name)
            content['customer'] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist."}, status=400)

        sale = Sale.objects.create(**content)
        return JsonResponse({"sale": sale}, encoder=SaleEncoder)


@require_http_methods(["GET", "DELETE"])
def api_sale(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False
            )
        except sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale does not exist"}
            )
    else:
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Sale does not exist"})

@require_http_methods("PUT")
def api_cars_update(request, vin):
    data = json.loads(request.body)
    AutomobileVO.objects.filter(vin=vin).update(**data)
    car = AutomobileVO.objects.get(vin=vin)
    return JsonResponse(
        car,
        encoder=AutomobileVOEncoder,
        safe=False,
        status=200,
    )
