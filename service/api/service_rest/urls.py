from django.urls import path, include
from .views import api_technician, api_technicians
from .views import (
    api_technicians,
    api_technician,
    api_appointments,
    api_appointment,
    finish_appointment,
    cancel_appointment,
)


urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("technicians/<int:pk>", api_technician, name="api_technician"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointments/<int:pk>", api_appointment, name="api_appointment"),
    path("appointments/<int:pk>/finish/",
        finish_appointment, name="finish_appointment"),
    path("appointments/<int:pk>/cancel/",
        cancel_appointment, name="cancel_appointment"),
]
