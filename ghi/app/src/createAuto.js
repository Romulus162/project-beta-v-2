import React, { useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom'

function CreateAuto(){
    const navigate = useNavigate()
    const [models, setModels] = useState([]);
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('')
    const [ID, setID] = useState('')

    const changeColor = event =>{
        const value = event.target.value;
        setColor(value);
    }
    const changeYear = event =>{
        const value = event.target.value;
        setYear(value);
    }
    const changeVin = event =>{
        const value = event.target.value;
        setVin(value)
    }
    const changeID = event =>{
        const value = event.target.value;
        setID(value)
    }

    const modelsData = async() =>{
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }
    }

    useEffect(() =>{
        modelsData();
    }, []);

    const handleSubmit = async (event)=>{
        event.preventDefault();

        const data = {}
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = ID;

        const url = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok){
            setColor('');
            setYear('');
            setVin('');
            setID('');
            navigate('/cars')

        };
    };

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Add a car to inventory</h1>
					<form onSubmit={handleSubmit} id="create-carModel-form">
						<div className="form-floating mb-3">
							<input value={color} onChange={changeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
							<label htmlFor="color">Color</label>
						</div>
						<div className="form-floating mb-3">
							<input value={year} onChange={changeYear} placeholder="Year" required type="number" name="year" id="year" className="form-control" />
							<label htmlFor="year">Year</label>
						</div>
						<div className="form-floating mb-3">
							<input value={vin} onChange={changeVin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
							<label htmlFor="vin">Vin</label>
						</div>
						<div className="mb-3">
							<select value={ID} onChange={changeID} required id="model_id" name="model_id" className="form-select">
								<option>Choose a model</option>
								{models.map(mod => {
									return (
										<option value={mod.id} key={mod.id}>
											{mod.name}
										</option>
									);
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	)
};

export default CreateAuto
