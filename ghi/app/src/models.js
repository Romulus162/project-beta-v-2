import React, {useEffect, useState} from 'react';

function ListModels() {

    const [models, setModels] = useState([]);

    useEffect(() => {
        // loadManufacturers();
        const loadModels = async () => {
            try{
                const response = await fetch("http://localhost:8100/api/models/");
                if (response.ok) {
                    const data = await response.json();
                    setModels(data.models)
                } else {
                    console.error("Error:", response.status);
                }
            } catch (error) {
                console.error('Error1:', error)
            }
        };

        loadModels();
    }, []);

    return (
        <div>
            <h2>Models</h2>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map((model) => {
                        return (
                            <tr key={model.id}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td><img src={model.picture_url} style={{maxWidth: '300px', height: 'auto'}}/></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );


}

export default ListModels;
