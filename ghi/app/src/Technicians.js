import React, {useEffect, useState} from 'react';

function ListTechnicians() {

const [technicians, setTechnicians] = useState([]);

    useEffect(() => {
        // loadManufacturers();
        const loadTechnicians = async () => {
            try{
                const response = await fetch("http://localhost:8080/api/technicians/");
                if (response.ok) {
                    const data = await response.json();
                    setTechnicians(data.technicians)
                } else {
                    console.error("Error:", response.status);
                }
            } catch (error) {
                console.error('Error1:', error)
            }
        };

        loadTechnicians();
    }, []);


    return (
        <div>
            <h2>Technicians</h2>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map((technician) => {
                        return (
                            <tr key={technician.id}>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                                <td>{technician.employee_id}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );


}

export default ListTechnicians;
