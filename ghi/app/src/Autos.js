import React, {useEffect, useState} from 'react';

function ListCars() {

    const [cars, setCars] = useState([]);

    const loadModels = async()=>{
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCars(data.autos);
        }
    };
    useEffect(()=>{
        loadModels();
    }, []);

    return (
        <div>
            <h2>Cars</h2>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Car</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {cars.map((car) => {
                        return (
                            <tr key={car.id}>
                                <td>{car.vin}</td>
                                <td>{car.color}</td>
                                <td>{car.year}</td>
                                <td>{car.model.name}</td>
                                <td>{car.model.manufacturer.name}</td>
                                <td>{car.sold.toString()}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );


}

export default ListCars;
