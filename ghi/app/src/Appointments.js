import React, { useEffect, useState } from 'react';


function ListAppointment() {
	const [appointments, setAppointments] = useState([]);

	const loadAppointments = async () => {
		const url = "http://localhost:8080/api/appointments/"
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setAppointments(data.appointments)
		}
	}
	useEffect(() => {
		loadAppointments();
	}, [])

	const handleCancelAppointment = async (id) => {
		const cancelAppointmentUrl = `http://localhost:8080/api/appointments/${id}/cancel/`;
		const fetchConfig = {
			method: "PUT",
			body: JSON.stringify({ "status": "cancelled" }),
			headers: {
				"Content-Type": "application.json",
			},
		};
		const response = await fetch(cancelAppointmentUrl, fetchConfig)
		if (response.ok) {
			loadAppointments();
		}
	};


	const handleFinishAppointment = async (id) => {
		const finishAppointmentUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
		const fetchConfig = {
			method: "PUT",
			body: JSON.stringify({ "status": "finished" }),
			headers: {
				"Content-Type": "application.json",
			},
		};
		const response = await fetch(finishAppointmentUrl, fetchConfig);
		if (response.ok) {
			loadAppointments();
		}
	};

	return (
		<div className="table-responsive">
			<h1>Appointments</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>VIN</th>
						<th>VIP</th>
						<th>Customer</th>
						<th>Date</th>
						<th>Time</th>
						<th>Technician</th>
						<th>Reason</th>
					</tr>
				</thead>
				<tbody>
					{appointments?.map(a => {
						if (a.status === "created") {
							return (
								<tr key={a.id}>
									<td>{a.vin}</td>
									<td>{a.vip}</td>
									<td>{a.customer}</td>
									<td>{new Date(a.date_time).toLocaleDateString()}</td>
									<td>{new Date(a.date_time).toLocaleTimeString()}</td>
									<td>{a.technician.first_name + ' ' + a.technician.last_name}</td>
									<td>{a.reason}</td>
									<td>
										<button className="btn btn-secondary" onClick={() => handleCancelAppointment(a.id)}>Cancel</button>
									</td>
									<td>
										<button className="btn btn-warning" onClick={() => handleFinishAppointment(a.id)}>Finish</button>
									</td>
								</tr>
							)
						};
					})}
				</tbody>
			</table>
		</div>
	);

};

export default ListAppointment
