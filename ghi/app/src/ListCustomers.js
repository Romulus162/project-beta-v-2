import { useState, useEffect } from 'react';

function ListCustomers() {
    const [customers, setCustomers] = useState([]);

    const fetchData = async () => {
        const response = await fetch('http://localhost:8090/api/customers/');
        if(response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div>
        <h2>Cars</h2>

        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone Number</th>
                    <th>Address</th>
                </tr>
            </thead>
            <tbody>
                {customers.map((person) => {
                    return(
                        <tr key={person.id}>
                            <td>{person.first_name}</td>
                            <td>{person.last_name}</td>
                            <td>{person.phone_number}</td>
                            <td>{person.address}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </div>
    )
}

export default ListCustomers;
