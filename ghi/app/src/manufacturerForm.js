import React, { useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom'

function CreateManufacturer () {

    const navigate = useNavigate()
    const [name, setName] = useState('')
    const changeManufacturereName = (event) =>{
        const value = event.target.value;
        setName(value);
    }

    const handleSubmit = async(event) =>{
        event.preventDefault();

        const data = {}
        data.name = name;

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        // const response = await fetch(manufacturerUrl, fetchConfig, {mode: 'no-cors'});
        const response = await fetch(manufacturerUrl, fetchConfig);

        if (response.ok) {
            // const newManufacturer = await response.json();
            setName('');
            navigate('/manufacturers');
            // console.log(newManufacturer);
            // setName('');
            // props.getManufacturers();
        };
    };

    // useEffect(() => {

    // }, []);



    // const handleNameChange = (event) =>{
    //     const value = event.target.value;
    //     setName(value);
    //   }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={changeManufacturereName} placeholder='Manufacturer Name' required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <button className='btn btn-primary'>Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
};

export default CreateManufacturer



// {
//     "name": "Chrysler"
//   }


// class Manufacturer(models.Model):
//     name = models.CharField(max_length=100, unique=True)

//     def get_api_url(self):
//         return reverse("api_manufacturer", kwargs={"pk": self.id})


// const handleSubmit = async (event) => {
//     event.preventDefault();

//     const data = {};
//     data.fabric = fabric;
//     data.style_name = style_name;
//     data.color = color;
//     data.picture_url = picture_url;
//     data.location = location;

//     const hatUrl = 'http://localhost:8090/api/hats/';
//     const fetchConfig = {
//       method: "post",
//       body: JSON.stringify(data),
//       headers: {
//         'Content-Type': 'application/json',
//       },
//     };
