import React, { useState, useEffect } from "react";

function History(){
    const [appointments, setAppointments] = useState([]);
    const [searchVIN, setSearchVIN] = useState("");
    const [filteredAppointments, setFilteredAppointments] = useState([]);

    const fetchData = async () => {
        const response = await fetch("http://localhost:8080/api/appointments/");
        if (response.ok){
            const data = await response.json();
            setAppointments(data.appointments);
            setFilteredAppointments(data.appointments);
        }
    };
    useEffect(() =>{
        fetchData();
    }, []);

    const search = e => {
        e.preventDefault();
        if (searchVIN){
            setFilteredAppointments(
                appointments.filter((a) =>
                a.vin.toLowerCase().includes(searchVIN.toLowerCase()))
            );
        } else{
            setFilteredAppointments(appointments)
        }
    };
    const handleInputChange = e => {
        setSearchVIN(e.target.value);
    };

    return (
        <div>
            <h1>Service History</h1>
            <form onSubmit={search}>
                <div className="form-group">
                    <label htmlFor="vin">Search by VIN</label>
                    <div className="input-group">
                        <input
                            type="text"
                            name="vin"
                            id="vin"
                            className="form-control"
                            value={searchVIN}
                            onChange={handleInputChange}
                            />
                            <div className="input-group-append">
                                <button type="submit" className="btn btn-primary">
                                    <i className="bi bi-search"></i> Search
                                </button>
                            </div>
                    </div>
                </div>
            </form>
            <div className="table-responsive">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer</th>
							<th>Date</th>
							<th>Time</th>
							<th>Technician</th>
							<th>Reason</th>
							<th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filteredAppointments.map((a) =>(
                            <tr key={a.id}>
                                <td>{a.vin}</td>
                                <td>{a.customer}</td>
								<td>{new Date(a.date_time).toLocaleDateString()}</td>
								<td>{new Date(a.date_time).toLocaleTimeString()}</td>
								<td>{a.technician.first_name} {a.technician.last_name}</td>
								<td>{a.reason}</td>
								<td>{a.status}</td>

                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default History;
