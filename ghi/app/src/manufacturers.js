import React, {useEffect, useState} from 'react';

function ListManufacturer() {

    const [manufacturers, setManufacturers] = useState([]);
    // const loadManufacturers = async () =>{
    //     const url = "http://localhost:8100/api/manufacturers/";
    //     const response = await fetch(url);
    //     if (response.ok) {
    //         const data = await response.json();
    //         setmanufacturers(data.manufacturers);
    //     }
    // };
    useEffect(() => {
        // loadManufacturers();
        const loadManufacturers = async () => {
            try{
                const response = await fetch("http://localhost:8100/api/manufacturers/");
                if (response.ok) {
                    const data = await response.json();
                    setManufacturers(data.manufacturers)
                } else {
                    console.error("Error:", response.status);
                }
            } catch (error) {
                console.error('Error1:', error)
            }
        };

        loadManufacturers();
    }, []);

    return (
        <div>
            <h2>Manufacturers</h2>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map((manufacturer) => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );


}

export default ListManufacturer;
